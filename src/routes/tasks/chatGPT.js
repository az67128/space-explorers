const API_URL = 'https://api.openai.com/v1/engines/text-davinci-002/completions';


async function generateSteps(task, apiKey) {
  const prompt = `Given a complex task "${task}", break it down into smaller steps. Stepes should be in language which provided in task description`;

  const response = await fetch(API_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${apiKey}`,
    },
    body: JSON.stringify({
      prompt,
      max_tokens: 1000,
      n: 1,
      stop: null,
      temperature: 0.5,
    }),
  });

  const data = await response.json();
  console.log(data)
  const steps = data.choices[0]?.text.trim().split('\n') || [];
  return steps.filter(step => step);
}

export { generateSteps };
